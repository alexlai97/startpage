// Search on enter key event
function search(e) {
  if (e.keyCode == 13) {
    var val = document.getElementById("search-field").value;
    window.open("https://duckduckgo.com/?q=" + val, '_self', false);
  }
}
// Get current time and format
function getTime() {
  let date = new Date(),
    min = date.getMinutes(),
    sec = date.getSeconds(),
    hour = date.getHours();

  return "" + 
    (hour < 10 ? ("0" + hour) : hour) + ":" + 
    (min < 10 ? ("0" + min) : min) + ":" + 
    (sec < 10 ? ("0" + sec) : sec);
}

window.onload = () => {
  let xhr = new XMLHttpRequest();
  // Request to open weather map
  xhr.open('GET', 'https://api.openweathermap.org/data/2.5/weather?id=6176823&units=metric&APPID=56e59484ed99d2c15a2688a4bd67f104');
  xhr.onload = () => {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        let json = JSON.parse(xhr.responseText);
        console.log(json);
        document.getElementById("temp").innerHTML = json.main.temp.toFixed(0) + " C";
        document.getElementById("weather-description").innerHTML = json.weather[0].description;
      } else {
        console.log('error msg: ' + xhr.status);
      }
    }
  }
  xhr.send();
  // Set up the clock
  document.getElementById("clock").innerHTML = getTime();
  // Set clock interval to tick clock
  setInterval( () => {
    document.getElementById("clock").innerHTML = getTime();
  },100);
}

document.addEventListener("keydown", event => {
  if (event.keyCode == 32) {          // Spacebar code to open search
    document.getElementById('search').style.display = 'flex';
    document.getElementById('search-field').focus();
  } else if (event.keyCode == 27) {   // Esc to close search
    document.getElementById('search-field').value = '';
    document.getElementById('search-field').blur();
    document.getElementById('search').style.display = 'none';
  }
});


